package com.Santhosh.SpringProject;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.Santhosh.SpringProject.Profile;
import com.Santhosh.SpringProject.BatStats;
import com.Santhosh.SpringProject.BowlStats;
import com.Santhosh.SpringProject.ProjectRepository;
import com.Santhosh.SpringProject.ProjectService;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.lang.String;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
@SpringBootTest
class SpringProjectApplicationTests {
	@Autowired
	private ProjectService projectService;
	@MockBean
	private ProjectRepository projectRepository;
	@Test
	public void getUsersTest(){
		List<Profile> profiles;
		Profile profile=new Profile();
		BatStats batStats=new BatStats();
		BowlStats bowlStats=new BowlStats();
		PersonalDetails personalDetails=new PersonalDetails();
		personalDetails.setId(1);
		personalDetails.setName("virat");
		personalDetails.setBatstyle("Right Hand");
		personalDetails.setBowlstyle("RightArm Medium fast");
		personalDetails.setHeight(170);
		personalDetails.setBirthplace("delhi");
		personalDetails.setRole("Batsman");
		batStats.setId(1);
		batStats.setCenturies(70);
		batStats.setHalfcenturies(200);
		batStats.setAvg(55.9);
		batStats.setRuns(11000);
		batStats.setMatches(230);
		batStats.setHighscore(400);
		batStats.setSr(100.01);
		bowlStats.setId(1);
		bowlStats.setEconomy(24.98);
		bowlStats.setFivewickethaul(2);
		bowlStats.setWickets(24);
		bowlStats.setMatches(230);
		bowlStats.setAvg(40);
		profile.setBowling(bowlStats);
		profile.setBatting(batStats);
		profile.setPersonalDetails(personalDetails);

		when(projectRepository.getAllProfiles()).thenReturn(Stream.of(profile).collect(Collectors.toList()));
		assertEquals(1,projectService.getAllProfiles().size());
	}
	@Test
	public void getProfileByIdTest(){
		String id ="1";
		Profile profile=new Profile();
		BatStats batStats=new BatStats();
		BowlStats bowlStats=new BowlStats();
		PersonalDetails personalDetails=new PersonalDetails();
		personalDetails.setId(1);
		personalDetails.setName("virat");
		personalDetails.setBatstyle("Right Hand");
		personalDetails.setBowlstyle("RightArm Medium fast");
		personalDetails.setHeight(170);
		personalDetails.setBirthplace("delhi");
		personalDetails.setRole("Batsman");
		batStats.setId(1);
		batStats.setCenturies(70);
		batStats.setHalfcenturies(200);
		batStats.setAvg(55.9);
		batStats.setRuns(11000);
		batStats.setMatches(230);
		batStats.setHighscore(400);
		batStats.setSr(100.01);
		bowlStats.setId(1);
		bowlStats.setEconomy(24.98);
		bowlStats.setFivewickethaul(2);
		bowlStats.setWickets(24);
		bowlStats.setMatches(230);
		bowlStats.setAvg(40);
		profile.setBowling(bowlStats);
		profile.setBatting(batStats);
		profile.setPersonalDetails(personalDetails);
		when(projectRepository.getPlayer(id)).thenReturn(profile);
		assertEquals(profile,projectRepository.getPlayer(id));
	}
	@Test
	public void addPlayerTest(){
		PersonalDetails personalDetails=new PersonalDetails();
		personalDetails.setId(1);
		personalDetails.setName("virat");
		personalDetails.setBatstyle("Right Hand");
		personalDetails.setBowlstyle("RightArm Medium fast");
		personalDetails.setHeight(170);
		personalDetails.setBirthplace("delhi");
		personalDetails.setRole("Batsman");
		projectService.addPersonalDetails(personalDetails);
		verify(projectRepository,times(1)).addPersonalDetails(personalDetails);
	}
	@Test
	public void deleteTest(){
		PersonalDetails personalDetails=new PersonalDetails();
		personalDetails.setId(1);
		personalDetails.setName("virat");
		personalDetails.setBatstyle("Right Hand");
		personalDetails.setBowlstyle("RightArm Medium fast");
		personalDetails.setHeight(170);
		personalDetails.setBirthplace("delhi");
		personalDetails.setRole("Batsman");
		projectService.deletePersonalDetails("1");
		verify(projectRepository,times(1)).deletePersonalDetails("1");
	}
}
