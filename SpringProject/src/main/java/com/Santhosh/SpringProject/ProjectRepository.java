package com.Santhosh.SpringProject;

import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Repository
public class ProjectRepository {
    public List<Profile> getAllProfiles(){
        List<Profile> profiles =new ArrayList<>();
        String query1="Select * from PersonalDetails";
        String query2="select * from testbat";
        String query3="select * from testbowl";
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            Statement statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery(query1);
            Statement statement1=connection.createStatement();
            ResultSet resultSet2=statement1.executeQuery(query2);
            Statement statement2=connection.createStatement();
            ResultSet resultSet3=statement2.executeQuery(query3);
            while (resultSet.next()&&resultSet2.next()&&resultSet3.next()){
                Profile profile=new Profile();
                PersonalDetails p=new PersonalDetails();
                BatStats b=new BatStats();
                BowlStats c=new BowlStats();
                p.setName(resultSet.getString(1));
                p.setBirthplace(resultSet.getString(2));
                p.setHeight(resultSet.getInt(3));
                p.setRole(resultSet.getString(4));
                p.setBatstyle(resultSet.getString(5));
                p.setBowlstyle(resultSet.getString(6));
                p.setId(resultSet.getInt(7));
                b.setMatches(resultSet2.getInt(1));
                b.setRuns(resultSet2.getInt(2));
                b.setHighscore(resultSet2.getInt(3));
                b.setAvg(resultSet2.getDouble(4));
                b.setSr(resultSet2.getDouble(5));
                b.setCenturies(resultSet2.getInt(6));
                b.setHalfcenturies(resultSet2.getInt(7));
                b.setId(resultSet2.getInt(8));
                c.setMatches(resultSet3.getInt(1));
                c.setRuns(resultSet3.getInt(2));
                c.setAvg(resultSet3.getDouble(3));
                c.setWickets(resultSet3.getInt(4));
                c.setEconomy(resultSet3.getDouble(5));
                c.setFivewickethaul(resultSet3.getInt(6));
                c.setId(resultSet3.getInt(7));
                profile.setPersonalDetails(p);
                profile.setBatting(b);
                profile.setBowling(c);
                profiles.add(profile);
            }
            connection.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return profiles;
    }
    public Profile getPlayer(String id){
        String query="Select * from PersonalDetails where id ="+id;
        String query2="select * from testbat where id ="+id;
        String query3="select * from testbowl where id ="+id;
        PersonalDetails p=new PersonalDetails();
        BatStats b=new BatStats();
        BowlStats c= new BowlStats();
        Profile profile=new Profile();
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            Statement statement=connection.createStatement();
            Statement statement1=connection.createStatement();
            Statement statement2=connection.createStatement();
            ResultSet resultSet=statement.executeQuery(query);
            ResultSet resultSet2=statement1.executeQuery(query2);
            ResultSet resultSet3=statement2.executeQuery(query3);
                if (resultSet.next()&&resultSet2.next()&&resultSet3.next()) {
                    p.setName(resultSet.getString(1));
                    p.setBirthplace(resultSet.getString(2));
                    p.setHeight(resultSet.getInt(3));
                    p.setRole(resultSet.getString(4));
                    p.setBatstyle(resultSet.getString(5));
                    p.setBowlstyle(resultSet.getString(6));
                    p.setId(resultSet.getInt(7));
                    b.setMatches(resultSet2.getInt(1));
                    b.setRuns(resultSet2.getInt(2));
                    b.setHighscore(resultSet2.getInt(3));
                    b.setAvg(resultSet2.getDouble(4));
                    b.setSr(resultSet2.getDouble(5));
                    b.setCenturies(resultSet2.getInt(6));
                    b.setHalfcenturies(resultSet2.getInt(7));
                    b.setId(resultSet2.getInt(8));
                    c.setMatches(resultSet3.getInt(1));
                    c.setRuns(resultSet3.getInt(2));
                    c.setAvg(resultSet3.getDouble(3));
                    c.setWickets(resultSet3.getInt(4));
                    c.setEconomy(resultSet3.getDouble(5));
                    c.setFivewickethaul(resultSet3.getInt(6));
                    c.setId(resultSet3.getInt(7));
                    profile.setPersonalDetails(p);
                    profile.setBatting(b);
                    profile.setBowling(c);
                }

            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return profile;
    }
    public void addPersonalDetails(PersonalDetails personalDetails){
        String query="insert into personaldetails values(?,?,?,?,?,?,?)";
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            PreparedStatement statement=connection.prepareStatement(query);
            statement.setString(1,personalDetails.getName());
            statement.setString(2,personalDetails.getBirthplace());
            statement.setInt(3,personalDetails.getHeight());
            statement.setString(4,personalDetails.getRole());
            statement.setString(5,personalDetails.getBatstyle());
            statement.setString(6,personalDetails.getBowlstyle());
            statement.setInt(7,personalDetails.getId());
            statement.executeUpdate();
            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void updatePersonalDetails(PersonalDetails personalDetails,String id) {
        String query = "update personaldetails set name=?,place=?,height=?,role=?,batstyle=?,bowlstyle=? where height =" + id;
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1,personalDetails.getName());
            statement.setString(2, personalDetails.getBirthplace());
            statement.setInt(3, personalDetails.getHeight());
            statement.setString(4, personalDetails.getRole());
            statement.setString(5, personalDetails.getBatstyle());
            statement.setString(6, personalDetails.getBowlstyle());
            statement.executeUpdate();
            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
    public void deletePersonalDetails(String id){
        String query="delete from personaldetails where id ="+id;
        String query2="delete from testbat where id ="+id;
        String query3="delete from testbowl where id ="+id;

        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            Statement statement=connection.createStatement();
            Statement statement1=connection.createStatement();
            Statement statement2=connection.createStatement();
            int rowseffected = statement.executeUpdate(query);
            int rowseffected2=statement1.executeUpdate(query2);
            int rowseffected3=statement2.executeUpdate(query3);
            connection.close();
            } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void addBatStats(BatStats batStats){
        String query="insert into testbat values(?,?,?,?,?,?,?,?)";
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            PreparedStatement statement=connection.prepareStatement(query);
            statement.setInt(1,batStats.getMatches());
            statement.setInt(2,batStats.getRuns());
            statement.setInt(3,batStats.getHighscore());
            statement.setDouble(4,batStats.getAvg());
            statement.setDouble(5,batStats.getSr());
            statement.setInt(6,batStats.getCenturies());
            statement.setInt(7,batStats.getHalfcenturies());
            statement.setInt(8,batStats.getId());
            statement.executeUpdate();
            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void addBowlStats(BowlStats bowlStats){
        String query= "insert into testbowl values(?,?,?,?,?,?,?)";
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            PreparedStatement statement=connection.prepareStatement(query);
            statement.setInt(1,bowlStats.getMatches());
            statement.setInt(2,bowlStats.getRuns());
            statement.setDouble(3,bowlStats.getAvg());
            statement.setInt(4,bowlStats.getWickets());
            statement.setDouble(5,bowlStats.getEconomy());
            statement.setInt(6,bowlStats.getFivewickethaul());
            statement.setInt(7,bowlStats.getId());
            statement.executeUpdate();
            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void addLoginDetails(LoginDetails loginDetails) {
        String query = "insert into admindetails values(?,?)";
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1,loginDetails.getUsername() );
            statement.setString(2,loginDetails.getPassword() );
            statement.executeUpdate();
            connection.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public boolean matchpass(LoginDetails loginDetails) {
        String username = loginDetails.getUsername();
        String password = loginDetails.getPassword();
        boolean result = false;
        String query = "select * from admindetails where username ="+"'"+username+"';";
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProjectDB", "postgres", "Santhu&2110");

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            String adminpassword = resultSet.getString(2);
            connection.close();
            if (password.equals(adminpassword)) {
                result = true;
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }
}
