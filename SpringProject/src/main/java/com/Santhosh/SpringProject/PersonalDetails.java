package com.Santhosh.SpringProject;

import org.springframework.beans.factory.annotation.Autowired;
public class PersonalDetails {
    private String name;
    @Autowired
    private String birthplace;
    private int height;
    private String batstyle;
    private String role;
    private String bowlstyle;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setBatstyle(String batstyle) {
        this.batstyle = batstyle;
    }

    public void setBowlstyle(String bowlstyle) {
        this.bowlstyle = bowlstyle;
    }

    public String getName() {
        return name;
    }
    public String getRole(){return role;}
    public String getBirthplace() {
        return birthplace;
    }
    public String getBowlstyle(){return bowlstyle;}
    public int getHeight() {
        return height;
    }

    public String getBatstyle() {
        return batstyle;
    }

}

