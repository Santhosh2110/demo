package com.Santhosh.SpringProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    public List<Profile> getAllProfiles(){
        List<Profile> profiles=projectRepository.getAllProfiles();
        return profiles;
    }
    public Profile getPlayer(String id){
        return projectRepository.getPlayer(id);
    }
    public void addPersonalDetails(PersonalDetails personalDetails){
        projectRepository.addPersonalDetails(personalDetails);
    }
    public void updatePersonalDetails(PersonalDetails personalDetails,String id){
        projectRepository.updatePersonalDetails(personalDetails,id);
    }
    public void deletePersonalDetails(String id){
        projectRepository.deletePersonalDetails(id);
    }
}
