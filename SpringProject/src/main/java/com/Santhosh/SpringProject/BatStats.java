package com.Santhosh.SpringProject;

import org.springframework.beans.factory.annotation.Autowired;

public class BatStats {

    private int matches;
    private int runs;
    private int highscore;
    private double sr;
    private int centuries;
    private int halfcenturies;
    private double avg;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatches() {
        return matches;
    }

    public int getRuns() {
        return runs;
    }

    public int getHighscore() {
        return highscore;
    }


    public double getSr() {
        return sr;
    }

    public int getCenturies() {
        return centuries;
    }

    public int getHalfcenturies() {
        return halfcenturies;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public void setHighscore(int highscore) {
        this.highscore = highscore;
    }

    public void setSr(double sr) {
        this.sr = sr;
    }

    public void setCenturies(int centuries) {
        this.centuries = centuries;
    }

    public void setHalfcenturies(int halfcenturies) {
        this.halfcenturies = halfcenturies;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public double getAvg() {
        return avg;
    }

    @Override
    public String toString() {
        return "BatStats{" +
                ", matches=" + matches +
                ", runs=" + runs +
                ", highscore=" + highscore +
                ", avg=" + avg+
                ", sr=" + sr +
                ", centuries=" + centuries +
                ", halfcenturies=" + halfcenturies +
                '}';
    }
}

