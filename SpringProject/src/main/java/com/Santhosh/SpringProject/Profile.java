package com.Santhosh.SpringProject;

import org.springframework.beans.factory.annotation.Autowired;

public class Profile {
    @Autowired
    private PersonalDetails personalDetails;
    private BatStats Batting;
    private BowlStats Bowling;
    public void setPersonalDetails(PersonalDetails personalDetails){
        this.personalDetails=personalDetails;
    }
    public void setBatting(BatStats batting){this.Batting=batting;}
    public void setBowling(BowlStats bowling){this.Bowling=bowling;}
    public PersonalDetails getPersonalDetails(){
        return this.personalDetails;
    }

    public BatStats getBatting() {
        return Batting;
    }

    public BowlStats getBowling() {
        return Bowling;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "personalDetails=" + personalDetails.toString() +
                '}';
    }
}
