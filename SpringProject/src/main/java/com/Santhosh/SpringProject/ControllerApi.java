package com.Santhosh.SpringProject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@Controller
public class ControllerApi {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProjectRepository projectRepository;
    @RequestMapping("/")
    public String getAllProfiles(){
        return "UserView";
    }
    @RequestMapping("/admin")
    public String getAllProfiles(Model model){
        model.addAttribute("profiles",projectRepository.getAllProfiles());
        return "admin";
    }
    @RequestMapping("/profile/{id}")
    public String getPlayerProfile(@PathVariable String id,Model model){
        Object ob=projectRepository.getPlayer(id);
        model.addAttribute("profile",ob);
        return "Cricinfo";
    }
    @RequestMapping("/personaldetails")
    public String showForm(Model model){
        PersonalDetails personalDetails=new PersonalDetails();
        model.addAttribute("personalDetails",personalDetails);
        return "Post";
    }
    @RequestMapping(method = RequestMethod.POST,value ="/personaldetails")
    public String addPlayer(@ModelAttribute PersonalDetails personalDetails){
        projectService.addPersonalDetails(personalDetails);
        return "redirect:/batstats";
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/personaldetails/{id}")
    public void updatePersonalDetails(@RequestBody PersonalDetails personalDetails,@PathVariable String id){
        projectService.updatePersonalDetails(personalDetails,id);
    }
    @GetMapping("/delete/{id}")
    public String deletePersonalDetails(@PathVariable String id){
        projectService.deletePersonalDetails(id);
        return "redirect:/admin";
    }
    @RequestMapping("/batstats")
    public String showFormBatstats(Model model){
        BatStats batStats=new BatStats();
        model.addAttribute("batStats",batStats);
        return "PostBat";
    }
    @RequestMapping(method = RequestMethod.POST,value ="/batstats")
    public String addBatstats(@ModelAttribute BatStats batStats){
        projectRepository.addBatStats(batStats);
        return "redirect:/bowlstats";
    }
    @RequestMapping("/bowlstats")
    public String showFormBowlstats(Model model){
        BowlStats bowlStats=new BowlStats();
        model.addAttribute("bowlStats",bowlStats);
        return "PostBowl";
    }
    @RequestMapping(method = RequestMethod.POST,value ="/bowlstats")
    public String addBowlstats(@ModelAttribute BowlStats bowlStats) {
        projectRepository.addBowlStats(bowlStats);
        return "redirect:/admin";
    }
    @RequestMapping("/signup")
    public String showLoginForm(Model model){
        LoginDetails loginDetails=new LoginDetails();
        model.addAttribute("loginDetails",loginDetails);
        return "SignUp";
    }
    @RequestMapping(method = RequestMethod.POST,value ="/signup")
    public String addAdmin(@ModelAttribute LoginDetails loginDetails){
        projectRepository.addLoginDetails(loginDetails);
        return "redirect:/signin";
    }
    @RequestMapping("/signin")
    public String showSignInForm(Model model){
        LoginDetails loginDetails=new LoginDetails();
        model.addAttribute("loginDetails",loginDetails);
        return "SignIn";
    }
    @RequestMapping(method = RequestMethod.POST,value ="/signin")
    public String loginAdmin(@ModelAttribute LoginDetails loginDetails){
        if (projectRepository.matchpass(loginDetails)){
            return "redirect:/admin";
        }
        else {
            return "redirect:/signin";
        }
    }
}
