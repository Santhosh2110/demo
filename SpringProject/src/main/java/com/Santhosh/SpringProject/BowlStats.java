package com.Santhosh.SpringProject;


public class BowlStats {
    private int matches;
    private int runs;
    private double avg;
    private int wickets;
    private double economy;
    private int fivewickethaul;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatches() {
        return matches;
    }

    public int getRuns() {
        return runs;
    }


    public double getAvg() {
        return avg;
    }

    public int getWickets() {
        return wickets;
    }

    public double getEconomy() {
        return economy;
    }

    public int getFivewickethaul() {
        return fivewickethaul;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }


    public void setAvg(double avg) {
        this.avg = avg;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public void setEconomy(double economy) {
        this.economy = economy;
    }

    public void setFivewickethaul(int fivewickethaul) {
        this.fivewickethaul = fivewickethaul;
    }

    @Override
    public String toString() {
        return "BowlStats{" +
                ", matches=" + matches +
                ", runs=" + runs +
                ", avg=" + avg +
                ", wickets=" + wickets +
                ", economy=" + economy +
                ", fivewickethaul=" + fivewickethaul +
                '}';
    }
}
